package de.julian98.fifo;

import de.julian98.PageRequests;

import java.util.HashMap;

public class FIFOPaging {

    public static int[] getFIFO(final int numberOfEntries, final long seed, final int numberOfPages,
                                final int from, final int to, final int locality) {
        HashMap<Integer, FIFOPage> pages = new HashMap<>(numberOfEntries);
        PageRequests pr = new PageRequests(seed, numberOfPages, from, to, locality);
        pr.setIndex(0);

        int cacheHits = 0;
        int cacheMisses = 0;

        int pageToStore;
        while ((pageToStore = pr.getNextPage()) != -1) {
            //Wenn Seite bereits existiert, wird Hit-Counter um 1 erhöht und nächste Seite betrachtet
            if (pages.containsKey(pageToStore)) {
                cacheHits++;
                continue;
            }

            cacheMisses++;

            //Wenn genug Platz, wird neue Seite einfach hinzugefügt
            if (pages.size() < numberOfEntries) {
                pages.put(pageToStore, new FIFOPage());
            } else {
                FIFOPage oldestFIFOPage = null;
                Integer oldestFIFOPageKey = null;

                //Älteste Seite wird ermittelt
                for (Integer key : pages.keySet()) {
                    FIFOPage page = pages.get(key);
                    if (oldestFIFOPage == null || oldestFIFOPage.age() < page.age()) {
                        oldestFIFOPage = page;
                        oldestFIFOPageKey = key;
                    }
                }

                //Älteste Seite (mit höchstem Alter) wird gelöscht und neue Seite hinzugefügt
                pages.remove(oldestFIFOPageKey);
                pages.put(pageToStore, new FIFOPage());
            }

            //Alter aller Seiten wird um 1 erhöht
            for (Integer key : pages.keySet())
                pages.get(key).increaseAge();
        }

        //Rückgabe der Hits und Misses für GUI
        return new int[] {
                cacheHits,
                cacheMisses
        };
    }

}