package de.julian98.fifo;

public class FIFOPage implements Comparable<FIFOPage> {

    private int age = -1;

    public FIFOPage() {
        resetAge();
    }

    public void resetAge() {
        age = 0;
    }

    public int age() {
        return age;
    }

    public void increaseAge() {
        if (age < Integer.MAX_VALUE) {
            age++;
        }
    }

    @Override
    public int compareTo(FIFOPage fPage) {
        if (fPage == null) {
            return 0;
        }

        return Integer.compare(age(), fPage.age());
    }

}
