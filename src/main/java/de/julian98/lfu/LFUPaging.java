package de.julian98.lfu;

import de.julian98.PageRequests;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class LFUPaging {

    public static int[] getLFU(final int numberOfEntries, final long seed, final int numberOfPages,
                               final int from, final int to, final int locality) {
        HashMap<Integer, LFUPage> pages = new HashMap<>(numberOfEntries);
        PageRequests pr = new PageRequests(seed, numberOfPages, from, to, locality);
        pr.setIndex(0);

        int cacheHits = 0;
        int cacheMisses = 0;

        int pageToStore;
        while ((pageToStore = pr.getNextPage()) != -1) {
            //Wenn Seite bereits existiert, wird Alter der Seite und Hit-Counter um 1 erhöht und nächste Seite betrachtet
            if (pages.containsKey(pageToStore)) {
                pages.get(pageToStore).increaseHits();
                cacheHits++;
                continue;
            }

            cacheMisses++;

            //Wenn genug Platz, wird neue Seite einfach hinzugefügt
            if (pages.size() < numberOfEntries) {
                pages.put(pageToStore, new LFUPage());
            } else {
                //Es wird ermittelt, wie viele Seiten die niedrigste aufgetretene Hit-Anzahl haben
                LFUPage pageWithLeastHits = null;
                for (Integer key : pages.keySet()) {
                    LFUPage page = pages.get(key);
                    if (pageWithLeastHits == null || pageWithLeastHits.hits() > page.hits()) {
                        pageWithLeastHits = page;
                    }
                }
                final int finalLeastHits = pageWithLeastHits.hits();

                Integer removableLeastHitsKey;
                List<Map.Entry<Integer, LFUPage>> entriesWithLeastHits = pages.entrySet().stream().filter(integerLFUPageEntry ->
                        integerLFUPageEntry.getValue().hits() == finalLeastHits).collect(Collectors.toList());
                //Wenn Ergebnis nicht eindeutig, wird älteste Seite aus in Frage kommenden Seiten nach FIFO-Prinzip bestimmt
                if (entriesWithLeastHits.size() > 1) {
                    Map.Entry<Integer, LFUPage> oldestPage = null;
                    for (Map.Entry<Integer, LFUPage> entryWithLeastHits : entriesWithLeastHits) {
                        if (oldestPage == null || oldestPage.getValue().age() < entryWithLeastHits.getValue().age()) {
                            oldestPage = entryWithLeastHits;
                        }
                    }
                    removableLeastHitsKey = oldestPage.getKey();
                } else {
                    //Wenn Ergebnis eindeutig, wird diese Seite gleich gelöscht
                    removableLeastHitsKey = entriesWithLeastHits.get(0).getKey();
                }

                //Seite mit geringster Hit-Anzahl bzw. älteste Seite nach FIFO-Prinzip wird entfernt und neue Seite wird hinzugefügt
                pages.remove(removableLeastHitsKey);
                pages.put(pageToStore, new LFUPage());
            }

            //Alter aller Seiten wird um 1 erhöht
            for (Integer key : pages.keySet())
                pages.get(key).increaseAge();
        }

        //Rückgabe der Hits und Misses für GUI
        return new int[] {
                cacheHits,
                cacheMisses
        };
    }

}
