package de.julian98.lfu;

public class LFUPage {

    private int age = -1;
    private int hits = -1;

    public LFUPage() {
        resetAge();
    }

    public void resetAge() {
        age = 0;
    }

    public int age() {
        return age;
    }

    public int hits() {
        return hits;
    }

    public void increaseAge() {
        if (age < Integer.MAX_VALUE) {
            age++;
        }
    }

    public void increaseHits() {
        if (hits < Integer.MAX_VALUE) {
            hits++;
        }
    }

}
