package de.julian98.nru;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import de.julian98.PageRequests;

import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class NRUPaging {

    public static int[] getNRU(final int numberOfEntries, final long seed, final int numberOfPages,
                               final int from, final int to, final int locality, final int reset) {
        HashMap<Integer, NRUPage> pages = new HashMap<>(numberOfEntries);
        PageRequests pr = new PageRequests(seed, numberOfPages, from, to, locality);
        pr.setIndex(0);

        int cacheHits = 0;
        int cacheMisses = 0;

        int pageToStore;
        int resetCounter = 0;
        while ((pageToStore = pr.getNextPage()) != -1) {
            resetCounter++;
            //Wenn Seite bereits existiert, wird Read-Counter und Hit-Counter erhöht
            if (pages.containsKey(pageToStore)) {
                pages.get(pageToStore).increaseReads();
                cacheHits++;

            } else {
                cacheMisses++;

                //Wenn genug Platz, wird Seite einfach hinzugefügt
                if (pages.size() < numberOfEntries) {
                    pages.put(pageToStore, new NRUPage());

                } else {
                    //Einteilung der Seiten in die vier Kategorien
                    Multimap<Integer, Integer> keysInCategories = ArrayListMultimap.create();
                    for (Integer key : pages.keySet()) {
                        NRUPage page = pages.get(key);
                        if (page.reads() == 0 && page.writes() == 0) {
                            keysInCategories.get(0).add(key);
                        } else if (page.reads() == 0 && page.writes() > 0) {
                            keysInCategories.get(1).add(key);
                        } else if (page.reads() > 0 & page.writes() == 0) {
                            keysInCategories.get(2).add(key);
                        } else if (page.reads() > 0 && page.writes() > 0) {
                            keysInCategories.get(3).add(key);
                        }
                    }
                    //Zufällige Seite aus möglichst "geringwertiger" Kategorie wird entfernt
                    for (int i = 0; i < 4; i++) {
                        List<Integer> list = (List<Integer>) keysInCategories.get(i);
                        if (list.size() > 0) {
                            pages.remove(list.get(ThreadLocalRandom.current().nextInt(list.size())));
                            break;
                        }
                    }

                    //Neue Seite wird hinzugefügt
                    pages.put(pageToStore, new NRUPage());
                }
            }

            //Reset beider Zähler aller Seiten nach jedem X Durchlauf
            if (resetCounter % reset == 0) {
                resetCounter = 0;
                for (Integer key : pages.keySet()) {
                    NRUPage page = pages.get(key);
                    page.reset();
                }
            }
        }

        //Rückgabe der Hits und Misses für GUI
        return new int[] {
                cacheHits,
                cacheMisses
        };
    }

}
