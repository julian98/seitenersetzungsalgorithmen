package de.julian98.nru;

public class NRUPage {

    private int writes;
    private int reads;

    public NRUPage() {
        writes = 1;
        reads = 0;
    }

    public void reset() {
        writes = 0;
        reads = 0;
    }

    public int writes() {
        return writes;
    }

    public int reads() {
        return reads;
    }

    public void increaseReads() {
        if (reads < Integer.MAX_VALUE) {
            reads++;
        }
    }

}
