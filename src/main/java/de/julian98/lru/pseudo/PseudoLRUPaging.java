package de.julian98.lru.pseudo;

import de.julian98.PageRequests;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

public class PseudoLRUPaging {

    public static int[] getPseudoLRU(final int numberOfEntries, final long seed, final int numberOfPages,
                                     final int from, final int to, final int locality) {
        HashMap<Integer, PseudoLRUPage> pages = new HashMap<>(numberOfEntries);
        PageRequests pr = new PageRequests(seed, numberOfPages, from, to, locality);
        pr.setIndex(0);

        int cacheHits = 0;
        int cacheMisses = 0;

        int pageToStore;
        while ((pageToStore = pr.getNextPage()) != -1) {
            //Wenn Seite bereits existiert, wird Hit-Counter um 1 erhöht und falls Alter der Seite == 0, wird Alter um 1 erhöht
            if (pages.containsKey(pageToStore)) {
                PseudoLRUPage pseudoLRUPage = pages.get(pageToStore);
                if (pseudoLRUPage.age() == 0) {
                    pseudoLRUPage.increaseAge();
                }
                cacheHits++;

            } else {
                cacheMisses++;

                //Wenn kein Platz für neue Seite, werden potentiell löschbare Seiten ermittelt, deren Alter == 0 ist
                if (pages.size() >= numberOfEntries) {
                    List<Integer> possibleRemovable = new ArrayList<>();
                    for (Integer key : pages.keySet()) {
                        if (pages.get(key).age() == 0) {
                            possibleRemovable.add(key);
                        }
                    }

                    //Zufällige Seite wird entfernt, dessen Alter == 0 ist
                    pages.remove(possibleRemovable.get(ThreadLocalRandom.current().nextInt(possibleRemovable.size())));
                }
            }

            //Wenn Seite noch nicht existiert, wird sie hinzugefügt
            if (!pages.containsKey(pageToStore)) {
                pages.put(pageToStore, new PseudoLRUPage());
            }

            //Wenn Alter aller Seiten == 1, wird das Alter aller Seiten resetted
            if (checkIfAllOne(pages)) {
                for (Integer key : pages.keySet()) {
                    if (numberOfEntries == 1 || key != pageToStore) {
                        pages.get(key).resetAge();
                    }
                }
            }
        }

        //Rückgabe der Hits und Misses für GUI
        return new int[] {
                cacheHits,
                cacheMisses
        };
    }

    private static boolean checkIfAllOne(Map<Integer, PseudoLRUPage> pages) {
        for (Integer key : pages.keySet()) {
            PseudoLRUPage page = pages.get(key);
            if (page.age() != 1) {
                return false;
            }
        }
        return true;
    }

}
