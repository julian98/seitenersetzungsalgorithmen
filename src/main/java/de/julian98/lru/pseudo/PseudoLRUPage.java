package de.julian98.lru.pseudo;

public class PseudoLRUPage implements Comparable<PseudoLRUPage> {

    private int age;

    public PseudoLRUPage() {
        age = 1;
    }

    public void resetAge() {
        age = 0;
    }

    public int age() {
        return age;
    }

    public void increaseAge() {
        if (age < Integer.MAX_VALUE) {
            age++;
        }
    }

    @Override
    public int compareTo(PseudoLRUPage pPage) {
        if (pPage == null) {
            return 0;
        }

        return Integer.compare(age(), pPage.age());
    }

}
