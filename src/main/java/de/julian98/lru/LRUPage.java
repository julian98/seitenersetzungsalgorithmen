package de.julian98.lru;

public class LRUPage implements Comparable<LRUPage> {

    private int age = -1;

    public LRUPage() {
        resetAge();
    }

    public void resetAge() {
        age = 0;
    }

    public int age() {
        return age;
    }

    public void increaseAge() {
        if (age < Integer.MAX_VALUE) {
            age++;
        }
    }

    @Override
    public int compareTo(LRUPage fPage) {
        if (fPage == null) {
            return 0;
        }

        return Integer.compare(age(), fPage.age());
    }

}