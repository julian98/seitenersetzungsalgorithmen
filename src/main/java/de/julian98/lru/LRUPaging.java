package de.julian98.lru;

import de.julian98.PageRequests;

import java.util.HashMap;

public class LRUPaging {

    public static int[] getLRU(final int numberOfEntries, final long seed, final int numberOfPages,
                               final int from, final int to, final int locality) {
        HashMap<Integer, LRUPage> pages = new HashMap<>(numberOfEntries);
        PageRequests pr = new PageRequests(seed, numberOfPages, from, to, locality);
        pr.setIndex(0);

        int cacheHits = 0;
        int cacheMisses = 0;

        int pageToStore;
        while ((pageToStore = pr.getNextPage()) != -1) {
            //Wenn Seite bereits existiert, wird Hit-Counter erhöht, Alter der Seite auf 0 gesetzt und nächste Seite betrachtet
            if (pages.containsKey(pageToStore)) {
                cacheHits++;
                pages.get(pageToStore).resetAge();
                continue;
            }

            cacheMisses++;

            //Wenn genug Platz, wird Seite einfach hinzugefügt
            if (pages.size() < numberOfEntries) {
                pages.put(pageToStore, new LRUPage());
            } else {
                LRUPage oldestLRUPage = null;
                Integer oldestLRUPageKey = null;

                //Älteste Seite wird ermittelt
                for (Integer key : pages.keySet()) {
                    LRUPage page = pages.get(key);
                    if (oldestLRUPage == null || oldestLRUPage.age() < page.age()) {
                        oldestLRUPage = page;
                        oldestLRUPageKey = key;
                    }
                }

                //Älteste Seite wird entfernt und neue Seite wird hinzugefügt
                pages.remove(oldestLRUPageKey);
                pages.put(pageToStore, new LRUPage());
            }

            //Alter aller Seiten wird um 1 erhöht
            for (Integer key : pages.keySet())
                pages.get(key).increaseAge();
        }

        //Rückgabe der Hits und Misses für GUI
        return new int[] {
                cacheHits,
                cacheMisses
        };
    }

}
