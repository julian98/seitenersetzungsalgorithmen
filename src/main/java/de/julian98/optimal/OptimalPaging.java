package de.julian98.optimal;

import de.julian98.PageRequests;

import java.util.HashMap;

public class OptimalPaging {

    public static int[] getOptimal(final long seed, final int numberOfRequests,
                                   final int from, final int to, final int locality) {
        HashMap<Integer, OptimalPage> pages = new HashMap<>();
        PageRequests pr = new PageRequests(seed, numberOfRequests, from, to, locality);
        pr.setIndex(0);

        int cacheHits = 0;
        int cacheMisses = 0;

        int pageToStore;
        while ((pageToStore = pr.getNextPage()) != -1) {
            //Wenn Seite bereits existiert, wird Hit-Counter erhöht und nächste Seite betrachtet
            if (pages.containsKey(pageToStore)) {
                cacheHits++;
                continue;
            }

            cacheMisses++;

            //Wenn Seite noch nicht existiert, wird Seite hinzugefügt
            pages.put(pageToStore, new OptimalPage());
        }

        //Rückgabe der Hits und Misses für GUI
        return new int[] {
                cacheHits,
                cacheMisses
        };

        /*System.out.println("Pages required : " + pages.size());
        System.out.println("Cache Hits   : " + cacheHits + " ( " + (double) cacheHits / (cacheHits + cacheMisses) * 100 + " % )");
        System.out.println("Cache Misses : " + cacheMisses + " ( " + (double) cacheMisses / (cacheHits + cacheMisses) * 100 + " % )");*/
    }

}
