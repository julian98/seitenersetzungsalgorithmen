package de.julian98;

import java.util.Random;

public class PageRequests {
    int[] pages = null;
    int next = -1;

    /**
     * creates a set of page requests
     *
     * @param random           (given) number generator
     * @param numberOfRequests
     * @param from
     * @param to
     * @param locality
     */
    private void setPages(Random random, int numberOfRequests, int from, int to, int locality) {
        assert (numberOfRequests >= 0);
        assert (from < to);

        pages = new int[numberOfRequests];
        for (int i = 0; i < numberOfRequests; i++) {
            if (i > 0 && locality > 0) {
                int localityRandom = random.nextBoolean() ? random.nextInt(locality + 1) : -1 * random.nextInt(locality + 1);

                pages[i] = Math.max(0, Math.min(to, pages[i - 1] + localityRandom));
            } else {
                pages[i] = random.nextInt(to + 1 - from) + from;
            }
        }
    }

    public PageRequests(int numberOfPages, int from, int to, int locality) {
        Random random = new Random();
        long seed = random.nextLong();
        random.setSeed(seed);
        System.out.println("Seed: " + seed);

        setPages(random, numberOfPages, from, to, locality);
    }

    public PageRequests(long seed, int numberOfPages, int from, int to, int locality) {
        setPages(new Random(seed), numberOfPages, from, to, locality);
    }

    public void setIndex(int next) {
        assert (pages != null);
        assert (next >= 0);
        assert (next < pages.length);

        this.next = next;
    }

    public int getIndex() {
        return next;
    }

    public int getNextPage() {
        if (next < pages.length) {
            return pages[next++];
        }

        return -1;
    }

    /**
     * @param es - element separation
     * @param li - number of elements for line interrupt
     * @param lb - number of elements for line break
     * @return composed String
     */
    public String toString(boolean es, int li, int lb) {
        assert (pages != null);

        StringBuilder sb = new StringBuilder();
        for (int i = 1; i < pages.length + 1; i++) {
            sb.append(pages[i - 1]);

            if (lb > 0 && i % lb == 0) {
                sb.append("\n");
            } else if (li > 0 && i % li == 0) {
                sb.append(" - ");
            } else if (es) {
                sb.append(" ");
            }
        }

        return sb.toString();
    }

    public String toString() {
        return toString(false, 0, 0);
    }

}
