package de.julian98.gui;

import de.julian98.fifo.FIFOPaging;
import de.julian98.lfu.LFUPaging;
import de.julian98.lifo.LIFOPaging;
import de.julian98.lru.LRUPaging;
import de.julian98.lru.pseudo.PseudoLRUPaging;
import de.julian98.nru.NRUPaging;
import de.julian98.optimal.OptimalPaging;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.StackedBarChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;

import java.awt.*;
import java.util.*;
import java.util.List;

public class GUI extends Application {

    private Image icon = new Image(getClass().getClassLoader().getResourceAsStream("icon.png"));

    private Random random = new Random();
    private Long seed;
    private List<Character> numbers = new ArrayList<>(Arrays.asList('0', '1', '2', '3', '4', '5', '6', '7', '8', '9'));

    private Button newSeed = new Button("Neuer Seed");
    private Label currentSeed = new Label();
    private Label numberOfPages = new Label("Anzahl der Seiten: ");
    private TextField numberOfPagesText = new TextField("1000");
    private Label from = new Label("Von: ");
    private TextField fromText = new TextField("1");
    private Label to = new Label("Bis: ");
    private TextField toText = new TextField("100");
    private Label locality = new Label("Lokalität: ");
    private TextField localityText = new TextField("5");
    private Label numberOfEntries = new Label("Anzahl der Einträge: ");
    private TextField numberOfEntriesText = new TextField("30");
    private Label numberOfRuns = new Label("Max. Durchläufe: ");
    private TextField numberOfRunsText = new TextField("6");

    private final String optimal = "Optimal";
    private final String fifo = "FIFO";
    private final String lifo = "LIFO";
    private final String lru = "LRU";
    private final String pseudoLRU = "PseudoLRU";
    private final String lfu = "LFU";
    private final String nru = "NRU";

    private XYChart.Series<String, Number> misses = new XYChart.Series<>();
    private XYChart.Data<String, Number> missesOptimal = new XYChart.Data<>(optimal, 1000);
    private XYChart.Data<String, Number> missesFIFO = new XYChart.Data<>(fifo, 1000);
    private XYChart.Data<String, Number> missesLIFO = new XYChart.Data<>(lifo, 1000);
    private XYChart.Data<String, Number> missesLRU = new XYChart.Data<>(lru, 1000);
    private XYChart.Data<String, Number> missesPseudoLRU = new XYChart.Data<>(pseudoLRU, 1000);
    private XYChart.Data<String, Number> missesLFU = new XYChart.Data<>(lfu, 1000);
    private XYChart.Data<String, Number> missesNRU = new XYChart.Data<>(nru, 1000);

    final XYChart.Series<String, Number> hits = new XYChart.Series<>();
    private XYChart.Data<String, Number> hitsOptimal = new XYChart.Data<>(optimal, 0);
    private XYChart.Data<String, Number> hitsFIFO = new XYChart.Data<>(fifo, 0);
    private XYChart.Data<String, Number> hitsLIFO = new XYChart.Data<>(lifo, 0);
    private XYChart.Data<String, Number> hitsLRU = new XYChart.Data<>(lru, 0);
    private XYChart.Data<String, Number> hitsPseudoLRU = new XYChart.Data<>(pseudoLRU, 0);
    private XYChart.Data<String, Number> hitsLFU = new XYChart.Data<>(lfu, 0);
    private XYChart.Data<String, Number> hitsNRU = new XYChart.Data<>(nru, 0);


    @Override
    public void start(Stage stage) {
        VBox root = new VBox();

        MenuBar menuBar = new MenuBar();
        Menu infoMenu = new Menu("Info");
        menuBar.getMenus().add(infoMenu);
        MenuItem infoMenuItem = new MenuItem("Autoren");
        infoMenu.getItems().add(infoMenuItem);
        root.getChildren().add(menuBar);
        infoMenuItem.setOnAction(event -> {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            Stage alertStage = (Stage) alert.getDialogPane().getScene().getWindow();
            alertStage.getIcons().add(icon);
            alert.setTitle("Autoren");
            alert.setHeaderText(null);
            alert.setContentText("Julian Scholz [802792]\nNoel Souvignier [809212]\nJonte Groen [803465]");
            alert.showAndWait();
        });

        final CategoryAxis xAxis = new CategoryAxis();
        xAxis.setLabel("Algorithmus");
        xAxis.tickLabelFontProperty().set(Font.font(13.0));
        final NumberAxis yAxis = new NumberAxis();
        yAxis.setLabel("Hit-/Miss-Rate");
        final StackedBarChart<String, Number> bc = new StackedBarChart<>(xAxis, yAxis);
        bc.setPadding(new Insets(10, 0, 10, 0));
        bc.setTitle("Algorithmen");

        misses.setName("Misses");
        misses.getData().setAll(missesOptimal, missesFIFO, missesLIFO, missesLRU, missesPseudoLRU, missesLFU, missesNRU);
        hits.setName("Hits");
        hits.getData().setAll(hitsOptimal, hitsFIFO, hitsLIFO, hitsLRU, hitsPseudoLRU, hitsLFU, hitsNRU);
        root.getChildren().add(bc);

        HBox wrapper = new HBox();
        wrapper.setPadding(new Insets(20, 0, 20, 0));
        wrapper.setBackground(new Background(new BackgroundFill(new Color(0.9, 0.9, 0.9, 1.0), CornerRadii.EMPTY, Insets.EMPTY)));
        wrapper.setAlignment(Pos.CENTER);

        VBox firstBox = new VBox();
        firstBox.setPadding(new Insets(0, 20, 0, 20));
        firstBox.setSpacing(5.0);
        newSeed.getStyleClass().add("big-font");
        newSeed.setOnAction(event -> {
            seed = random.nextLong();
            currentSeed.setText("Aktueller Seed: " + GUI.this.seed);
            drawAlgorithms(
                    Integer.parseInt(numberOfEntriesText.getText()),
                    seed,
                    Integer.parseInt(numberOfPagesText.getText()),
                    Integer.parseInt(fromText.getText()),
                    Integer.parseInt(toText.getText()),
                    Integer.parseInt(localityText.getText()),
                    Integer.parseInt(numberOfRunsText.getText())
            );
        });
        firstBox.getChildren().add(newSeed);
        currentSeed.getStyleClass().add("big-font");
        currentSeed.setMinWidth(260.0);
        seed = random.nextLong();
        currentSeed.setText("Aktueller Seed: " + this.seed);
        firstBox.getChildren().add(currentSeed);
        HBox firstBoxWrapper1 = new HBox();
        numberOfPages.setMinWidth(123.0);
        numberOfPages.getStyleClass().add("big-font");
        firstBoxWrapper1.getChildren().add(numberOfPages);
        numberOfPagesText.setTextFormatter(new TextFormatter<String>((TextFormatter.Change change) -> {
            if (change.getControlNewText().equals("")) {
                change.setText("0");
            } else if (change.getControlNewText().length() > 6 || !isNumeric(change.getControlNewText())) {
                return null;
            }
            drawAlgorithms(
                    Integer.parseInt(numberOfEntriesText.getText()),
                    seed,
                    Integer.parseInt(change.getControlNewText()),
                    Integer.parseInt(fromText.getText()),
                    Integer.parseInt(toText.getText()),
                    Integer.parseInt(localityText.getText()),
                    Integer.parseInt(numberOfRunsText.getText())
            );
            return change;
        }));
        firstBoxWrapper1.getChildren().add(numberOfPagesText);
        firstBox.getChildren().add(firstBoxWrapper1);
        wrapper.getChildren().add(firstBox);

        VBox secondBox = new VBox();
        secondBox.setPadding(new Insets(0, 20, 0, 20));
        secondBox.setSpacing(5.0);
        HBox secondBoxWrapper1 = new HBox();
        from.getStyleClass().add("big-font");
        from.setMinWidth(65.0);
        secondBoxWrapper1.getChildren().add(from);
        fromText.setTextFormatter(new TextFormatter<String>((TextFormatter.Change change) -> {
            if (change.getControlNewText().equals("")) {
                change.setText("1");
            } else if (change.getControlNewText().length() > 6 || !isNumeric(change.getControlNewText()) || Integer.parseInt(change.getControlNewText()) == 0 || Integer.parseInt(change.getControlNewText()) > Integer.parseInt(toText.getText())) {
                return null;
            }
            drawAlgorithms(
                    Integer.parseInt(numberOfEntriesText.getText()),
                    seed,
                    Integer.parseInt(numberOfPagesText.getText()),
                    Integer.parseInt(change.getControlNewText()),
                    Integer.parseInt(toText.getText()),
                    Integer.parseInt(localityText.getText()),
                    Integer.parseInt(numberOfRunsText.getText())
            );
            return change;
        }));
        secondBoxWrapper1.getChildren().add(fromText);
        secondBox.getChildren().add(secondBoxWrapper1);
        HBox secondBoxWrapper2 = new HBox();
        to.getStyleClass().add("big-font");
        to.setMinWidth(65.0);
        secondBoxWrapper2.getChildren().add(to);
        toText.setTextFormatter(new TextFormatter<String>((TextFormatter.Change change) -> {
            if (change.getControlNewText().equals("")) {
                change.setText("1");
            } else if (change.getControlNewText().length() > 6 || !isNumeric(change.getControlNewText()) || Integer.parseInt(change.getControlNewText()) == 0 || Integer.parseInt(change.getControlNewText()) < Integer.parseInt(fromText.getText())) {
                return null;
            }
            drawAlgorithms(
                    Integer.parseInt(numberOfEntriesText.getText()),
                    seed,
                    Integer.parseInt(numberOfPagesText.getText()),
                    Integer.parseInt(fromText.getText()),
                    Integer.parseInt(change.getControlNewText()),
                    Integer.parseInt(localityText.getText()),
                    Integer.parseInt(numberOfRunsText.getText())
            );
            return change;
        }));
        secondBoxWrapper2.getChildren().add(toText);
        secondBox.getChildren().add(secondBoxWrapper2);
        HBox secondBoxWrapper3 = new HBox();
        locality.getStyleClass().add("big-font");
        locality.setMinWidth(65.0);
        secondBoxWrapper3.getChildren().add(locality);
        localityText.setTextFormatter(new TextFormatter<String>((TextFormatter.Change change) -> {
            if (change.getControlNewText().equals("")) {
                change.setText("0");
            } else if (change.getControlNewText().length() > 6 || !isNumeric(change.getControlNewText())) {
                return null;
            }
            drawAlgorithms(
                    Integer.parseInt(numberOfEntriesText.getText()),
                    seed,
                    Integer.parseInt(numberOfPagesText.getText()),
                    Integer.parseInt(fromText.getText()),
                    Integer.parseInt(toText.getText()),
                    Integer.parseInt(change.getControlNewText()),
                    Integer.parseInt(numberOfRunsText.getText())
            );
            return change;
        }));
        secondBoxWrapper3.getChildren().add(localityText);
        secondBox.getChildren().add(secondBoxWrapper3);
        wrapper.getChildren().add(secondBox);

        VBox thirdBox = new VBox();
        thirdBox.setPadding(new Insets(0, 20, 0, 20));
        thirdBox.setSpacing(5.0);
        HBox thirdBoxWrapper1 = new HBox();
        numberOfEntries.setMinWidth(135.0);
        numberOfEntries.getStyleClass().add("big-font");
        thirdBoxWrapper1.getChildren().add(numberOfEntries);
        numberOfEntriesText.setTextFormatter(new TextFormatter<String>((TextFormatter.Change change) -> {
            if (change.getControlNewText().equals("")) {
                change.setText("1");
            } else if (change.getControlNewText().length() > 6 || !isNumeric(change.getControlNewText()) || Integer.parseInt(change.getControlNewText()) == 0) {
                return null;
            }
            drawAlgorithms(
                    Integer.parseInt(change.getControlNewText()),
                    seed,
                    Integer.parseInt(numberOfPagesText.getText()),
                    Integer.parseInt(fromText.getText()),
                    Integer.parseInt(toText.getText()),
                    Integer.parseInt(localityText.getText()),
                    Integer.parseInt(numberOfRunsText.getText())
            );
            return change;
        }));
        thirdBoxWrapper1.getChildren().add(numberOfEntriesText);
        thirdBox.getChildren().add(thirdBoxWrapper1);
        HBox thirdBoxWrapper2 = new HBox();
        numberOfRuns.setMinWidth(135.0);
        numberOfRuns.getStyleClass().add("big-font");
        thirdBoxWrapper2.getChildren().add(numberOfRuns);
        numberOfRunsText.setTextFormatter(new TextFormatter<String>((TextFormatter.Change change) -> {
            if (change.getControlNewText().equals("")) {
                change.setText("1");
            } else if (change.getControlNewText().length() > 6 || !isNumeric(change.getControlNewText()) || Integer.parseInt(change.getControlNewText()) == 0) {
                return null;
            }
            drawAlgorithms(
                    Integer.parseInt(numberOfEntriesText.getText()),
                    seed,
                    Integer.parseInt(numberOfPagesText.getText()),
                    Integer.parseInt(fromText.getText()),
                    Integer.parseInt(toText.getText()),
                    Integer.parseInt(localityText.getText()),
                    Integer.parseInt(change.getControlNewText())
            );
            return change;
        }));
        thirdBoxWrapper2.getChildren().add(numberOfRunsText);
        thirdBox.getChildren().add(thirdBoxWrapper2);

        wrapper.getChildren().add(thirdBox);
        root.getChildren().add(wrapper);

        Scene scene = new Scene(root, 900, 540);
        scene.getStylesheets().add(Objects.requireNonNull(getClass().getClassLoader().getResource("style.css")).toExternalForm());
        bc.getData().setAll(misses, hits);
        stage.setResizable(false);
        stage.getIcons().add(icon);
        stage.setTitle("Seitenersetzungsalgorithmen");
        stage.setScene(scene);
        stage.show();

        for (XYChart.Series<String, Number> serie : bc.getData()) {
            for (XYChart.Data<String, Number> item : serie.getData()) {
                item.getNode().setOnMousePressed((MouseEvent event) -> {
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    Stage alertStage = (Stage) alert.getDialogPane().getScene().getWindow();
                    alertStage.getIcons().add(icon);
                    alert.setTitle(item.getXValue() + "-Algorithmus");
                    int hitsCount = hits.getData().stream().filter(stringNumberData -> stringNumberData.getXValue().equals(item.getXValue())).findFirst().get().getYValue().intValue();
                    int missesCount = misses.getData().stream().filter(stringNumberData -> stringNumberData.getXValue().equals(item.getXValue())).findFirst().get().getYValue().intValue();
                    alert.setHeaderText("Hits: " + hitsCount + "\nMisses: " + missesCount);
                    double hitRate = (double) hitsCount / (missesCount + hitsCount) * 100;
                    double missRate = (double) missesCount / (missesCount + hitsCount) * 100;
                    alert.setContentText("Hit-Rate: " + (Math.round(hitRate * 100.0) / 100.0) + "%" + "\nMiss-Rate: " + (Math.round(missRate * 100.0) / 100.0) + "%");

                    Point mouseLocation = MouseInfo.getPointerInfo().getLocation();
                    alert.setX(mouseLocation.getX());
                    alert.setY(mouseLocation.getY());
                    alert.showAndWait();
                });
            }
        }

        drawAlgorithms(
                Integer.parseInt(numberOfEntriesText.getText()),
                seed,
                Integer.parseInt(numberOfPagesText.getText()),
                Integer.parseInt(fromText.getText()),
                Integer.parseInt(toText.getText()),
                Integer.parseInt(localityText.getText()),
                Integer.parseInt(numberOfRunsText.getText())
        );
    }

    public static void main(String[] args) {
        launch(args);
    }

    public void drawAlgorithms(int numberOfEntries, long seed, int numberOfPages, int from, int to, int locality, int numberOfRuns) {
        drawOptimal(seed, numberOfPages, from, to, locality);
        drawFIFO(numberOfEntries, seed, numberOfPages, from, to, locality);
        drawLIFO(numberOfEntries, seed, numberOfPages, from, to, locality);
        drawLRU(numberOfEntries, seed, numberOfPages, from, to, locality);
        drawPseudoLRU(numberOfEntries, seed, numberOfPages, from, to, locality);
        drawLFU(numberOfEntries, seed, numberOfPages, from, to, locality);
        drawNRU(numberOfEntries, seed, numberOfPages, from, to, locality, numberOfRuns);
    }

    public void drawOptimal(long seed, int numberOfPages, int from, int to, int locality) {
        int[] values = OptimalPaging.getOptimal(seed, numberOfPages, from, to, locality);
        hitsOptimal.setYValue(values[0]);
        missesOptimal.setYValue(values[1]);
    }

    public void drawFIFO(int numberOfEntries, long seed, int numberOfPages, int from, int to, int locality) {
        int[] values = FIFOPaging.getFIFO(numberOfEntries, seed, numberOfPages, from, to, locality);
        hitsFIFO.setYValue(values[0]);
        missesFIFO.setYValue(values[1]);
    }

    public void drawLIFO(int numberOfEntries, long seed, int numberOfPages, int from, int to, int locality) {
        int[] values = LIFOPaging.getLIFO(numberOfEntries, seed, numberOfPages, from, to, locality);
        hitsLIFO.setYValue(values[0]);
        missesLIFO.setYValue(values[1]);
    }

    public void drawLRU(int numberOfEntries, long seed, int numberOfPages, int from, int to, int locality) {
        int[] values = LRUPaging.getLRU(numberOfEntries, seed, numberOfPages, from, to, locality);
        hitsLRU.setYValue(values[0]);
        missesLRU.setYValue(values[1]);
    }

    public void drawPseudoLRU(int numberOfEntries, long seed, int numberOfPages, int from, int to, int locality) {
        int[] values = PseudoLRUPaging.getPseudoLRU(numberOfEntries, seed, numberOfPages, from, to, locality);
        hitsPseudoLRU.setYValue(values[0]);
        missesPseudoLRU.setYValue(values[1]);
    }

    public void drawLFU(int numberOfEntries, long seed, int numberOfPages, int from, int to, int locality) {
        int[] values = LFUPaging.getLFU(numberOfEntries, seed, numberOfPages, from, to, locality);
        hitsLFU.setYValue(values[0]);
        missesLFU.setYValue(values[1]);
    }

    public void drawNRU(int numberOfEntries, long seed, int numberOfPages, int from, int to, int locality, int numberOfRuns) {
        int[] values = NRUPaging.getNRU(numberOfEntries, seed, numberOfPages, from, to, locality, numberOfRuns);
        hitsNRU.setYValue(values[0]);
        missesNRU.setYValue(values[1]);
    }

    public boolean isNumeric(String text) {
        for (char c : text.toCharArray()) {
            if (!numbers.contains(c)) {
                return false;
            }
        }
        return true;
    }

}
