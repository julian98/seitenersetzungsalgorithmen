package de.julian98.lifo;

public class LIFOPage implements Comparable<LIFOPage> {

    private int age = -1;

    public LIFOPage() {
        resetAge();
    }

    public void resetAge() {
        age = 0;
    }

    public int age() {
        return age;
    }

    public void increaseAge() {
        if (age < Integer.MAX_VALUE) {
            age++;
        }
    }

    @Override
    public int compareTo(LIFOPage lPage) {
        if (lPage == null) {
            return 0;
        }

        return Integer.compare(age(), lPage.age());
    }

}
