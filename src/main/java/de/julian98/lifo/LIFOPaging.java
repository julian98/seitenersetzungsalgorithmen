package de.julian98.lifo;

import de.julian98.PageRequests;

import java.util.HashMap;

public class LIFOPaging {

    public static int[] getLIFO(final int numberOfEntries, final long seed, final int numberOfPages,
                                final int from, final int to, final int locality) {
        HashMap<Integer, LIFOPage> pages = new HashMap<>(numberOfEntries);
        PageRequests pr = new PageRequests(seed, numberOfPages, from, to, locality);
        pr.setIndex(0);

        int cacheHits = 0;
        int cacheMisses = 0;

        int pageToStore;
        while ((pageToStore = pr.getNextPage()) != -1) {
            //Wenn Seite bereits existiert wird Hit-Counter um 1 erhöht und nächste Seite betrachtet
            if (pages.containsKey(pageToStore)) {
                cacheHits++;
                continue;
            }

            cacheMisses++;

            //Wenn genug Platz, wird Seite einfach hinzugefügt
            if (pages.size() < numberOfEntries) {
                pages.put(pageToStore, new LIFOPage());
            } else {
                LIFOPage newestLIFOPage = null;
                Integer newestLIFOPageKey = null;

                //Neueste Seite (mit geringstem Alter) wird ermittelt
                for (Integer key : pages.keySet()) {
                    LIFOPage page = pages.get(key);
                    if (newestLIFOPage == null || newestLIFOPage.age() > page.age()) {
                        newestLIFOPage = page;
                        newestLIFOPageKey = key;
                    }
                }

                //Neuste Seite wird gelöscht und neue Seite wird hinzugefügt
                pages.remove(newestLIFOPageKey);
                pages.put(pageToStore, new LIFOPage());
            }

            //Alter aller Seiten wird um 1 erhöht
            for (Integer key : pages.keySet())
                pages.get(key).increaseAge();
        }

        //Rückgabe der Hits und Misses für GUI
        return new int[] {
                cacheHits,
                cacheMisses
        };
    }

}
